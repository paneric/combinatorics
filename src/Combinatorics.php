<?php

declare(strict_types=1);

namespace Paneric\Combinatorics;

class Combinatorics{

    private $count;

    private $result;

    public function getVariationsWithRepetitions(array $loops): array
    {
        $this->result = [];

        $this->setVariationsWithRepetitions($loops, 0, count($loops), []);

        return $this->result;
    }

    public function getVariationsWithRepetitionsPaginated(array $loops, int $limit, int $offset): array
    {
        $this->result = [];

        $this->setVariationsWithRepetitions($loops, 0, count($loops), []);

        return array_slice($this->result, $offset, $limit, true);
    }

    public function countVariationsWithRepetitions(array $loops): int
    {
        if (empty($loops)) {
            return 0;
        }

        $count = 1;

        foreach ($loops as $number) {
            $count *= $number;
        }

        $this->count = $count;

        return $count;
    }

    private function setVariationsWithRepetitions(array $loops, int $iLoop, int $nLoops, array $variation)
    {
        if ($iLoop < $nLoops) {
            for ($i = 0; $i < $loops[$iLoop]; $i++) {
                $variation[$iLoop] = $i + 1;

                $this->setVariationsWithRepetitions($loops, $iLoop + 1, $nLoops, $variation);

                if ($iLoop === $nLoops-1) {
                    $this->result[] = $variation;
                }
            }
        }
    }
}