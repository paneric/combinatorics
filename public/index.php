<?php

declare(strict_types=1);

use Paneric\Combinatorics\Combinatorics;

define('ENV', 'dev');

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require ROOT_FOLDER . 'vendor/autoload.php';

$combinatorics = new Combinatorics();
$variations = $combinatorics->getVariationsWithRepetitions([3, 3, 3, 3, 3]);

var_dump($variations);